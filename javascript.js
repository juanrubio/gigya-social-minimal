/** *****************************************************/
//                   MAIN FUNCTION
/** *****************************************************/
/**
 * Onload Method.
 @returns
 */
window.onload = function () {

    /* Actions associated to events */
    // console.log("on load limpio");

    /* Actions associated to events */
    gigya.accounts.addEventHandlers({
        // onAfterSubmit: onAfterSubmit,
        // onAfterScreenLoad: onAfterScreenLoad,
       }
    );
}
/*******************************************************/


function loginWithRaaS() {

    console.log('loggining...');

    // Redirects directly using URL
    gigya.accounts.showScreenSet({
        screenSet:'Default-RegistrationLogin',
        // screenSet:'simple-screen-set',
        startScreen:'gigya-login-screen',

        // Attached events (Will override the ones on the UI Builder!!! )
        //
        // redirectURL: "http://localhost/nestle-minimal/target.html"

    });

    /* Actions associated to events */
    gigya.accounts.addEventHandlers({
        onLogin: onLogin,
        onLogout: onLogout
       }
    );
    return false;
}


function registerWithRaaS() {

    console.log('registering...');

    // Redirects directly using URL
    gigya.accounts.showScreenSet({
        screenSet:'Default-RegistrationLogin',
        // screenSet:'simple-screen-set',
        startScreen:'gigya-register-screen',

        // Attached events (Will override the ones on the UI Builder!!! )
        onAfterScreenLoad: onAfterScreenLoad,
    });

    /* Actions associated to events */
    gigya.accounts.addEventHandlers({
        onLogin: onLogin,
        onLogout: onLogout,
       }
    );
    return false;
}

function updateProfile() {

    console.log('updating...');

    // Redirects directly using URL
    gigya.accounts.showScreenSet({
        screenSet:'Default-ProfileUpdate',
        startScreen:'gigya-update-profile-screen',
    });

    /* Actions associated to events */
    gigya.accounts.addEventHandlers({
        onLogin: onLogin,
        onLogout: onLogout,
       }
    );
    return false;
}

// Handle logout response
function onLogin(response) {
    if ( response.screen !== null && response.profile !== null) {

        // User Logged
        console.log('User has logged in');

        // Applying jQuery
        $('.message').addClass( "logged" );
        $('.message').text( "Logged with " + response.provider);
        // debugger;

        // Explicity redirect if onLogin was called.
        // window.location.href="";
    }
        else {
        alert('Error :');
    }
}

// Handle logout response
function onLogout(response) {

    if ( response.eventName === 'logout' ) {
        console.log('User has logged out');

        // Applying jQuery
        $('.message').removeClass( "logged" );
        $('.message').text( "Not Logged" );
    }
    else {
        alert('Error :' + response.errorMessage);
    }
}

// Handle onAfterScreenLoad
function onAfterScreenLoad (response) {
    console.log("on After Screen Load event triggered.");
}
